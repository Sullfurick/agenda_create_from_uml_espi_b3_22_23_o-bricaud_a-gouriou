# AGENDA CREATE FROM URL
### Développé par Olivier BRICAUD & Aurélien GOURIOU

Cet exercice de développement est réalisé dans le cadre du cours "Modélisation UML" dispensé par Erwann Duclos
@Docusland à l'EPSI Rennes en 2023.
L'objectif est de développer un programme qui répond aux attentes du cahier des charges, et surtout, 
du diagramme UML fourni.
Le langage ainsi que toutes les librairies sont libres.

#### Langage : Python 🐍
#### Framework : Flask 🧪

### Rappel du cahier des charges :
##### Nous désirons implanter la gestion d’un agenda :

- Un agenda contient un ensemble de personnes nommées "Contacts".
- Un agenda possède un Owner possédant un login/mot de passe et JWT Token.
- Chaque contact est identifié par son nom et par un ensemble de coordonnées.
- Une coordonnée peut être postale, téléphonique ou électronique (email ou page web).
- Chaque coordonnée possède une méthode propre permettant de valider le format de saisie.

![Class_diag.png](Class_diag.png)

--------------------------------------------------------------------

```bash
git clone https://gitlab.com/Sullfurick/agenda_create_from_uml_espi_b3_22_23_o-bricaud_a-gouriou.git
cd agenda_create_from_uml_espi_b3_22_23_o-bricaud_a-gouriou
```

```shell
python main.py

# 127.0.0.1:5001/home => register puis login
# disclaimer : attention, avoir un postgres et verifier les parametres host, port, user, password etc

# docker compose up pas fini (mais ca reglera le disclaimer précédent)
```

En l'état :

- Bibliothèques de classes et méthodes (disponible dans `main.py`, méthode `demo_process()`)
- Pipeline de test gitlab (`test/test_unitaires.py` avec `pytest`)
- Persistence des données pour créer un utilisateur et s'authentifier

En travaux :

- Persistence des données Contacts et Agendas
- Docker et docker compose

### Schema d'architecture

```mermaid
graph LR
    
    subgraph front 

    flask_render_template

    end

    subgraph back 
        subgraph flask
            subgraph _class
                User
                Contact
            end
            subgraph services
                is_valid
            end
            subgraph class_database
                DB
            end
            subgraph route
                /login
                /register
                /add_contact
            end
        end
    end

    subgraph db 
        subgraph postgres
            table_agenda_user
            table_contact
            table_agenda_has_contact
        end
    end
flask_render_template --> /register --> User --> DB --> table_agenda_user
flask_render_template --> /login --> User --> DB --> table_agenda_user
flask_render_template --> /add_contact --> Contact --> is_valid --> DB --> table_contact
```

### Digramme de classes

```mermaid
classDiagram
    class User
    User : +String username
    User : -String password
    User : +open_agenda(self)
    User : +register_contact_in_agenda(self, agenda, new_contact)
    User : +create_contact(self, new_name, new_phone, new_mail, new_website, new_address)
    User : +update_contact(self, agenda, contact_id, updated_phone=None, updated_mail=None)
    User : +delete_contact(self, agenda, contact_id)
    User : +call_contact(self, agenda, contact_id)

    class Contact
    Contact : +String name
    Contact : -String mail
    Contact : -String phone
    Contact : -String address
    Contact : -String website
    Contact : +fill_mail(self, new_mail)
    Contact : +fill_phone(self, new_phone)
    Contact : +fill_address(self, new_address)
    Contact : +fill_website(self, new_website)

    class ContactDetailsAbstract
    ContactDetailsAbstract : +String value
    ContactDetailsAbstract : +String regex_patternt
    ContactDetailsAbstract : is_valid(self)



    class ContactDetails
    ContactDetails : +String specified_regex


    ContactDetails <|-- ContactDetailsAbstract
    Contact "0-4" *-- "1" ContactDetails

    class Agenda
    Agenda : +String user
    Agenda : -List contacts
    Agenda : +nb_contacts(self)
    Agenda : +get_contact(self, contact_id)
    Agenda : +remove_contact(self, contact_id)

    Agenda "1" *-- "*" User
    Agenda "*" *-- "*" Contact
   
```

```mermaid
classDiagram
    class Database
    Database : +String host
    Database : +String post
    Database : +get_connexion(self)
    Database : +add_contact(self)
    Database : +get_contact(self)

```

### SQL

```sql
CREATE TABLE agenda_user (
   id serial PRIMARY KEY,
   username varchar(100) UNIQUE NOT NULL,
   password varchar(100) NOT NULL
);

CREATE TABLE contact (
   id serial PRIMARY KEY,
   name varchar(100) UNIQUE NOT NULL,
   address varchar(150) NOT NULL,
   mail varchar(40) NOT NULL,
   phone varchar(12) NOT NULL
);


CREATE TABLE agenda_contact (
   id serial PRIMARY KEY,
   user_id serial NOT NULL,
   contact_id serial NOT NULL,
   CONSTRAINT fk
      FOREIGN KEY(user_id) 
	  REFERENCES agenda_user(id),
      FOREIGN KEY(contact_id) 
	  REFERENCES contact(id)
);

ALTER TABLE contact
   ALTER COLUMN address TYPE varchar(150),
   ALTER COLUMN mail TYPE varchar(40),
   ALTER COLUMN phone TYPE varchar(12);


```


