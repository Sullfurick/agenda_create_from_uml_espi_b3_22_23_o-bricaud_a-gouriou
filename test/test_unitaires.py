import pytest

from src import Mail, Phone, User, Agenda, logger

logger.init()


#  Format validation test

def test_valid_mail():
    m = Mail(value='bob@bob.com')
    assert m.is_valid()


def test_not_valid_mail():
    m = Mail(value='bob@@@bob.com')
    assert not m.is_valid()


def test_valid_phone():
    p = Phone(value='01-01-01-01-01')
    assert p.is_valid()


def test_not_valid_phone():
    p = Phone(value='01-01-01-01-')
    assert not p.is_valid()


# User
def test_create_contact():
    u = User(username='bob', password='1234')

    new_c = u.create_contact(
        new_name='orel',
        new_mail='a@a.com',
        new_phone='01-01-01-01-0',
        new_address='epsi',
        new_website='orel.com'
    )

    assert new_c.__str__() == '|Contact : orel, Infos : epsi - None - a@a.com - orel.com|'


def test_create_user_agenda_contact():
    u = User(username='bob', password='1234')

    a = Agenda(user=u)

    new_c = u.create_contact(
        new_name='orel',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )

    a.add_contact(new_c)
    assert a.nb_contacts() == 1


def test_create_contact_not_my_agenda(caplog):

    _name = 'bob'

    u = User(username=_name, password='1234')
    v = User(username='bobby', password='1234')

    a = Agenda(user=u)

    new_c = u.create_contact(
        new_name='orel',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )

    with pytest.raises(AssertionError):
        assert v.register_contact_in_agenda(agenda=a, new_contact=new_c)

    assert caplog.records[0].msg == f'{_name} creating contact'
    assert caplog.records[1].msg == 'NOT UR AGENDA'


def test_call_a_contact():
    """
    On peut appeler un contact existant dans un de nos agendas
    :return:
    """
    u = User(username='bob', password='1234')
    a = Agenda(user=u)
    new_c1 = u.create_contact(
        new_name='orel1',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )
    new_c2 = u.create_contact(
        new_name='orel2',
        new_mail='a@a.com',
        new_phone='02-02-02-02-02',
        new_address='epsi',
        new_website='orel.com'
    )
    new_c3 = u.create_contact(
        new_name='orel3',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )
    a.add_contact(new_c1)
    a.add_contact(new_c2)
    a.add_contact(new_c3)

    res = u.call_contact(a, 1)
    assert res == 'bob calling orel2 at 02-02-02-02-02'


def test_call_a_void_contact(caplog):
    """
    On ne peut pas appeler un contact inexistant
    :param caplog:
    :return:
    """
    # logger.init()
    u = User(username='bob', password='1234')
    a = Agenda(user=u)
    u.call_contact(a, 1)
    assert caplog.records[0].msg == 'Cant find this contact'


def test_update_contact_valid_update():
    """
    On peut modifier un contact avec une nouvelle valeur dont le format est valide
    :return:
    """

    _name = 'bob'

    u = User(username=_name, password='1234')

    a = Agenda(user=u)

    new_c = u.create_contact(
        new_name='orel',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )

    u.register_contact_in_agenda(agenda=a, new_contact=new_c)

    updated_phone = Phone(value='01-23-45-67-89')
    u.update_contact(agenda=a, contact_id=0, updated_phone=updated_phone)

    expected = '|Contact : orel, Infos : epsi - 01-23-45-67-89 - a@a.com - orel.com|'
    assert a.get_contact(0).__str__() == expected, 'La modification n\'a pas eu lieu'


def test_update_contact_not_valid_update():
    """
    On ne peut pas modifier un contact avec une nouvelle valeur dont le format ne correspond pas
    :return:
    """
    _name = 'bob'

    u = User(username=_name, password='1234')

    a = Agenda(user=u)

    new_c = u.create_contact(
        new_name='orel',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )

    u.register_contact_in_agenda(agenda=a, new_contact=new_c)

    updated_phone = Phone(value='abcd')
    u.update_contact(agenda=a, contact_id=0, updated_phone=updated_phone)

    expected = '|Contact : orel, Infos : epsi - 01-01-01-01-01 - a@a.com - orel.com|'
    assert a.get_contact(0).__str__() == expected, 'la modification ne devrait pas avoir eu lieu'


def test_remove_contact():
    _name = 'bob'

    u = User(username=_name, password='1234')

    a = Agenda(user=u)

    new_c1 = u.create_contact(
        new_name='orel',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )
    new_c2 = u.create_contact(
        new_name='orel2',
        new_mail='a@a.com',
        new_phone='01-01-01-01-01',
        new_address='epsi',
        new_website='orel.com'
    )

    u.register_contact_in_agenda(agenda=a, new_contact=new_c1)
    u.register_contact_in_agenda(agenda=a, new_contact=new_c2)

    u.delete_contact(agenda=a, contact_id=0)

    assert a.get_contact(0).name == 'orel2'
    assert a.nb_contacts() == 1