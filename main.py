from src import User, Agenda, Server, Phone


def demo_process():
    u = User(username='bob', password='1234')
    print(u)

    a = Agenda(user=u)
    print(a)

    new_c1 = u.create_contact(new_name='orel', new_mail='a@a.com', new_phone='01-01-01-01-01', new_address='epse',
                              new_website='orel.com')
    new_c2 = u.create_contact(new_name='erwann', new_mail='a@a.com', new_phone='01-01-01-01-01', new_address='epse',
                              new_website='orel.com')
    new_c3 = u.create_contact(new_name='bobby', new_mail='a@a.com', new_phone='01-01-01-01-01', new_address='epse',
                              new_website='orel.com')
    print(new_c1)

    a.add_contact(new_c1)
    a.add_contact(new_c2)
    a.add_contact(new_c3)
    print(a)

    u.call_contact(agenda=a, contact_id=1)

    u.update_contact(a, 1, updated_phone=Phone('89-76-54-32-10'))

    print(a)

    u.delete_contact(a, contact_id=0)

    print(a)


def api_process():
    api = Server()
    api.start()


if __name__ == '__main__':
    demo_process()
