import re


class ContactDetails:
    def __init__(self, value):
        # self.value = ContactDetailFactory.create(value)

        self.value = value
        self._regex_pattern = r''

    def is_valid(self):
        """
        Validate the new contact detail format with regex
        :return:
        """
        _regex = re.compile(self._regex_pattern)
        return re.fullmatch(_regex, self.value)

