import logging

from src.regex_pattern import MAIL_REGEX, PHONE_REGEX


class Contact:
    def __init__(self, name):
        self.name = name
        self.__address = ''
        self.__phone = ''
        self.__mail = ''
        self.__website = None

    def fill_address(self, new_address):
        self.__address = new_address.value

    def fill_phone(self, new_phone):
        if new_phone.is_valid():
            self.__phone = new_phone.value
        else:
            logging.warning('Invalid phone format')

    def fill_mail(self, new_mail):
        if new_mail.is_valid():
            self.__mail = new_mail.value
        else:
            logging.warning('Invalid mail format')

    def fill_website(self, new_web_site):
        self.__website = new_web_site.value

    @property
    def phone(self):
        return self.__phone

    @property
    def address(self):
        return self.__address

    @property
    def mail(self):
        return self.__mail

    def __str__(self):
        return f'|Contact : {self.name}, Infos : {self.__address} - {self.__phone} - {self.__mail} - {self.__website}|'

    def __repr__(self):
        return self.__str__()
