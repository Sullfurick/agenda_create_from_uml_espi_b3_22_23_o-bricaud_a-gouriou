import logging


class Agenda:
    def __init__(self, user):
        self.user = user
        self.__contacts = []

    def add_contact(self, new_contact):
        self.__contacts.append(new_contact)

    def nb_contacts(self):
        return len(self.__contacts)

    def get_contact(self, contact_id):
        try:
            return self.__contacts[contact_id]
        except IndexError as e:
            logging.info('Cant find this contact')
            return None

    def remove_contact(self, contact_id):
        try:
            self.__contacts.pop(contact_id)
        except IndexError as e:
            logging.info('Cant find & remove this contact')
            return None
    def __str__(self):
        return f'============\nje suis l\'agenda de {self.user} :\n{self.__contacts}\n======='
