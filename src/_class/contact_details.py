from src._class import ContactDetails
from src.regex_pattern import MAIL_REGEX, PHONE_REGEX, WEBSITE_REGEX


class Address(ContactDetails):

    def __str__(self):
        return f'Address : {self.value}'

    def __repr__(self):
        return self.__str__()


class Phone(ContactDetails):

    def __init__(self, value):
        super().__init__(value)
        self._regex_pattern = PHONE_REGEX

    def __str__(self):
        return f'Phone : {self.value}'


class Mail(ContactDetails):

    def __init__(self, value):
        super().__init__(value)
        self._regex_pattern = MAIL_REGEX

    def __str__(self):
        return f'Mail : {self.value}'


class WebSite(ContactDetails):

    def __init__(self, value):
        super().__init__(value)
        self._regex_pattern = WEBSITE_REGEX

    def __str__(self):
        return f'WebSite : {self.value}'
