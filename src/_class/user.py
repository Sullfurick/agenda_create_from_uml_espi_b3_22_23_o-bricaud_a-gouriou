import logging

from .contact import Contact

from .contact_details import Mail, Phone, Address, WebSite


class User:
    def __init__(self, username, password):
        self.username = username
        self.__password = password
        self.__jwt_token = None

    @property
    def password(self):
        return self.__password

    def open_agenda(self, agenda):
        """
        On ne peut intéragir sur un agenda que si on en est le proprietaire
        :param agenda:
        :return:
        """
        try:
            assert agenda.user == self
        except AssertionError:
            logging.info('NOT UR AGENDA')

    def register_contact_in_agenda(self, agenda, new_contact):
        self.open_agenda(agenda)
        agenda.add_contact(new_contact)

    def create_contact(self, new_name, new_phone, new_mail, new_website, new_address):
        logging.info(f'{self.username} creating contact')

        tmp_contact = Contact(name=new_name)

        tmp_mail = Mail(value=new_mail)
        tmp_contact.fill_mail(tmp_mail)
        tmp_phone = Phone(value=new_phone)
        tmp_contact.fill_phone(tmp_phone)
        tmp_website = WebSite(value=new_website)
        tmp_contact.fill_website(tmp_website)
        tmp_address = Address(value=new_address)
        tmp_contact.fill_address(tmp_address)

        return tmp_contact

    def update_contact(self, agenda, contact_id, updated_phone=None, updated_mail=None):

        logging.info(f'{self.username} creating contact')

        self.open_agenda(agenda)

        tmp_contact = agenda.get_contact(contact_id)
        if tmp_contact:
            if updated_phone:
                tmp_contact.fill_phone(updated_phone)
            if updated_mail:
                tmp_contact.fill_mail(updated_mail)

    def delete_contact(self, agenda, contact_id):
        agenda.remove_contact(contact_id)

    def call_contact(self, agenda, contact_id):
        current_contact = agenda.get_contact(contact_id)
        if current_contact:
            return f'{self.username} calling {current_contact.name} at {current_contact.phone}'
        return ''

    def __str__(self):
        return f'User {self.username}'
