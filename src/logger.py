import logging


def init():
    log_format = logging.Formatter('%(levelname)-8s - %(module)-25s -> %(funcName)-25s: %(message)s')
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_format)

    logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger('urllib3').setLevel(logging.INFO)
    logging.getLogger().addHandler(stream_handler)
