# Import des bibliotheques standard
import json
from time import sleep

import psycopg2
from psycopg2.extras import RealDictCursor

from src import User


# Import des ressources custom


# Classe
class Database:
    """
    Objet Database pour assurer la communication
    entre l'app flask/python et la base de donnees postgresql

    :ivar _conf: configuration reseau (ip, port)
    :ivar _vault: parametres d'authentification
    """

    ###################### CONSTRUCTOR ######################
    def __init__(self):
        """
        Instancie un nouvel objet base de donnees
        """
        self.__dbHost = '0.0.0.0'
        self.__dbPort = '5432'
        self.__dbName = 'agenda_uml'
        self.__dbUser = 'postgres'
        self.__dbPassword = 'postgres'

    ###################### DEBUG ######################
    def show(self):
        """
        Methode d'affichage d'un objet instancie de la classe Database
        :return:
        """
        print(f'User : {self.__dbUser}, host : {self.__dbHost} : {self.__dbPort}')

    # end show

    ###################### initial connection ######################
    def get_connection(self):
        """
        Ouverture d'une connexion a la base de donnees (parametres dans Conf.py et Vault.py)
        :return:
        """
        res = None
        while res is None:
            print("***DB connection***")
            try:
                res = psycopg2.connect(
                    host=self.__dbHost,
                    port=self.__dbPort,
                    user=self.__dbUser,
                    password=self.__dbPassword,
                )
            except Exception as e:
                print(e)
            sleep(2)
        return res

    # end get_connection

    def register_user(self, new_user: User):
        """
        Enregistrement d'un nouvel utilisateur

        :param new_user: le nouvel utilisateur à ajouter en base de donnees

        :return: int id de creation
        :rtype: int

        :raises Exeption e (sql syntax, connexion issue)
        """
        new_username = new_user.username
        new_password = new_user.password
        query = f"INSERT INTO agenda_user (username, password) VALUES ('{new_username}', '{new_password}') RETURNING id;"
        self.execute_query(query)

    def add_contact(self, new_contact):
        """
        Add contact
        """
        new_name = new_contact.name
        query = f"INSERT INTO contact (name, phone, mail, address) VALUES ('{new_name}', '{new_contact.phone}', '{new_contact.mail}', '{new_contact.address}') RETURNING id;"
        res = self.execute_query(query)
        return res

    def get_user(self, user: User):
        query = f"SELECT * FROM agenda_user WHERE username = '{user.username}' AND password = '{user.password}'"
        res = self.execute_query(query)
        return res

    def execute_query(self, query):
        """
        Methode generale d'execution d'une requete sql

        :param query: requete sql
        :type query: str

        :return:
        """
        conn = self.get_connection()
        cur = conn.cursor()
        cur.execute(query)
        res = cur.fetchall()
        cur.close()
        conn.commit()
        return res
