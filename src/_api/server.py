import logging

from flask import Flask, render_template, request
from flask_cors import CORS

from .database import Database
from .conf import SERVER_HOST, SERVER_PORT
from src import User, logger, Contact, ContactDetails, Address, Phone, Mail


class Server:
    def __init__(self):
        """
        Instancie un nouvel objet serveur
        Possede un attribut database qui est une instance de la classe Database
        """
        self.__host = SERVER_HOST
        self.__port = SERVER_PORT
        self.__db = Database()

    def start(self):
        """
        Method generale de fonctionnement et de demarrage du serveur
        On y definit ici les routes et les reponses associees
        """
        ############# CONF #############

        print("***start flask***")
        app = Flask(__name__)
        logger.init()

        # some conf
        CORS(app)
        app.config['DEBUG'] = True
        app.config['CORS_HEADER'] = 'Content-Type'

        ############# DATABASE #############
        conn = self.__db.get_connection()
        print(conn)
        ############# ROUTES #############
        @app.route('/')
        def root():
            return "root"

        @app.route('/home')
        def home():
            return render_template('home.html')

        @app.route('/register', methods=['GET', 'POST'])
        def register():
            if request.method == 'POST':
                username = request.form['username']
                password = request.form['password']
                logging.info(f'REGISTER {username} - {password}')
                new_user = User(username=username, password=password)
                self.__db.register_user(new_user)
                return render_template('home.html')
            if request.method == 'GET':
                return render_template('register.html')

        @app.route('/login', methods=['GET', 'POST'])
        def login():
            if request.method == 'POST':
                username = request.form['username']
                password = request.form['password']
                logging.info(f'LOGIN {username} - {password}')
                user = User(username=username, password=password)
                check_user = self.__db.get_user(user)
                if check_user:
                    return render_template('agenda.html')
                return render_template('home.html')
            if request.method == 'GET':
                return render_template('login.html')

        @app.route('/add_contact', methods=['POST'])
        def add_contact():
            name = request.form['name']
            address = request.form['address']
            mail = request.form['email']
            phone = request.form['phone']
            logging.info(f'ADD {name} - {address} - {mail} - {phone}')
            contact = Contact(name=name)
            contact.fill_mail(Phone(phone))
            contact.fill_mail(Address(address))
            contact.fill_mail(Mail(mail))
            self.__db.add_contact(contact)
            return render_template("agenda.html")

        app.run(host=self.__host, port=self.__port)


