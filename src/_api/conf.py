import os

SERVER_HOST = '0.0.0.0'
SERVER_PORT = '5001'

DATABASE_HOST = os.getenv("DATABASE_HOST", "localhost")
DATABASE_PORT = '5432'
